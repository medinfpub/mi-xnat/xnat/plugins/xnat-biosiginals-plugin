package eu.umg.mi.xnat.plugins;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;
import org.nrg.xdat.bean.BiosignalsEdfsessiondataBean;
import org.nrg.xdat.bean.BiosignalsEdfscandataBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
        name        = "XNAT Biosignals Plugin",
        value       = "xnat-biosignals-plugin",
        description = "Biosignals",

        dataModels = {
                @XnatDataModel(
                        value    = BiosignalsEdfsessiondataBean.SCHEMA_ELEMENT_NAME,
                        singular = "EDF Session",
                        plural   = "EDF Sessions",
                        code     = "EDF"
                ),
                @XnatDataModel(
                        value    = BiosignalsEdfscandataBean.SCHEMA_ELEMENT_NAME,
                        singular = "EDF Scan",
                        plural   = "EDF Scans"
                )
        }
)

public class BiosignalsPlugin {
}
