XNAT Biosignals Plugin
======================

XNAT plugin providing schema's and views for biosignal data.

Features
--------

* EDF Session and Scan data types:
  - `biosignals:edfSessionData`
  - `biosignals:edfScanData`
* Clients:
  - Web based EDF viewer and uploader: [xnat-edf-client](https://gitlab.gwdg.de/medinfpub/mi-xnat/xnat/clients/xnat-edf-client)

Build
-----

- First build and install the client

```
cd xnat-edf-client/
export NODE_OPTIONS=--openssl-legacy-provider
npm install
npm run build

cd ..
cp xnat-edf-client/build/* src/main/resources/META-INF/resources/EDFCLIENT

```

- Then build with the plugin with gradle

```
./gradlew clean xnatPluginjar 
```

- The resulting plugin jar will located at `build/libs/xnat-biosignals-plugin-x.x.x.jar` 

Install
-------

- Download the latest release, or build the plugin from source
- Copy `xnat-biosignals-plugin-x.x.x.jar` to `<xnat_home>/plugins`
- Restart tomcat

